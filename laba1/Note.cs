﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba1
{
    public class Note
    {
        public static int Max_id { get; set; }
        public int Id { get; set; }
        public string Sname { get; set; }
        public string Name { get; set; }
        public string Otchestvo { get; set; }
        public string Phone_number { get; set; }
        public string Country { get; set; }
        public string Birth_date { get; set; }
        public string Organisation { get; set; }
        public string Position { get; set; }
        public string Other_notes { get; set; }
        public static bool brake = false;

        public Note(int id, string Name, string Sname, string Otchestvo, string Phone_number, string Country, string Birth_date, string Organisation, string Position, string Other_notes)
        {
            this.Id = id;
            this.Sname = Sname;
            this.Name = Name;
            this.Otchestvo = Otchestvo;
            this.Phone_number = Phone_number;
            this.Country = Country;
            this.Birth_date = Birth_date;
            this.Organisation = Organisation;
            this.Position = Position;
            this.Other_notes = Other_notes;
        }
    }
   
 }

