﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba1
{
    public class Notebook
    {
        public static bool brake = false;

        public List<Note> notebookNotes;

        public List<Note> NotebookNotes
        {
            get { return notebookNotes; }
            set { notebookNotes = value; }
        }



        public Notebook()
        {
            notebookNotes = new List<Note>();
        }




        public void AddNote(int Id, string Sname, string Name, string Otchestvo, string Phone_number, string Country, string Birth_date, string Organisation, string Position, string Other_notes)
        {
            
            Id = ++Note.Max_id;

            while (string.IsNullOrEmpty(Name))
            {
                Console.WriteLine("введите Имя ");
                Name = Convert.ToString(Console.ReadLine());
            }


            while (string.IsNullOrEmpty(Sname))
            {
                Console.WriteLine("введите Фамилию ");
                Sname = Convert.ToString(Console.ReadLine());
            }

            Console.WriteLine("введите Отчество ");
            Otchestvo = Convert.ToString(Console.ReadLine());


            while (string.IsNullOrEmpty(Phone_number))
            {
                Console.WriteLine("введите Номер телефона ");
                Phone_number = Convert.ToString(Console.ReadLine());
            }

            while (string.IsNullOrEmpty(Country))
            {
                Console.WriteLine("введите Страну проживания ");
                Country = Convert.ToString(Console.ReadLine());
            }


            Console.WriteLine("введите дату рождения ");
            Birth_date = Convert.ToString(Console.ReadLine());

            Console.WriteLine("введите Название организации");
            Organisation = Convert.ToString(Console.ReadLine());

            Console.WriteLine("введите Должность ");
            Position = Convert.ToString(Console.ReadLine());

            Console.WriteLine("для заметок");
            Other_notes = Convert.ToString(Console.ReadLine());

            Note note = new Note(Id, Sname, Name, Otchestvo, Phone_number, Country, Birth_date, Organisation, Position, Other_notes);
            notebookNotes.Add(note);
            Console.WriteLine("Id={0}, Sname={1}, Name={2}, Otchestvo={3}, Phone_number={4}, Country={5}, Birth_date={6}, Organisation={7}, Position={8}, Other_notes={9})", note.Id, note.Sname, note.Name, note.Otchestvo, note.Phone_number, note.Country, note.Birth_date, note.Organisation, note.Position, note.Other_notes);
        }






        public List<Note> SearchNotes(int Id, string Sname, string Name, string Otchestvo, string Phone_number, string Country, string Birth_date, string Organisation, string Position, string Other_notes)
        {

            string intValue = string.Empty;
            while (string.IsNullOrEmpty(intValue))
            {
                Console.Write(" Введите id записи ");
                var userInput = Console.ReadLine();
                if (int.TryParse(userInput, out var parsedValue))
                {
                    intValue = Convert.ToString(parsedValue);
                }
                else
                {
                    Console.WriteLine("введите число");
                }
            }


              List<Note> ret = new List<Note>();
              

            foreach (Note note in notebookNotes)
                if (note.Id == Convert.ToUInt32(intValue))
                    ret.Add(note);

            foreach (Note note in ret)
            Console.WriteLine("На данном Id находися следующая запись: Id={0}, Sname={1}, Name={2}, Otchestvo={3}, Phone_number={4}, Country={5}, Birth_date={6}, Organisation={7}, Position={8}, Other_notes={9})", note.Id, note.Sname, note.Name, note.Otchestvo, note.Phone_number, note.Country, note.Birth_date, note.Organisation, note.Position, note.Other_notes);

            return ret;
            
        }


  

        public void PrintNotes()
        {

            foreach (Note c in notebookNotes)
            {
                Console.WriteLine(" Sname={0}, Name={1}, Phone_number={2}, id={3}",  c.Sname, c.Name, c.Phone_number, c.Id);
            }
        }



        public void DeleteNotes(int Id, string Sname, string Name, string Otchestvo, string Phone_number, string Country, string Birth_date, string Organisation, string Position, string Other_notes)
        {
            List<Note> Notes = SearchNotes(Id, Sname, Name, Otchestvo, Phone_number, Country, Birth_date, Organisation, Position, Other_notes);
            foreach (Note note in Notes)
                notebookNotes.Remove(note);
        }





        public void EditNotes (int Id, string Sname, string Name, string Otchestvo, string Phone_number, string Country, string Birth_date, string Organisation, string Position, string Other_notes)
        {

            string intValue = string.Empty;
            while (string.IsNullOrEmpty(intValue))
            {
                Console.Write(" Введите id записи ");
                var userInput = Console.ReadLine();
                if (int.TryParse(userInput, out var parsedValue))
                {
                    intValue = Convert.ToString(parsedValue);
                }
                else
                {
                    Console.WriteLine("введите число");
                }
            }



            Console.WriteLine("введите новую информацию");

            Name = Convert.ToString(Console.ReadLine());
            Console.WriteLine("введите Фамилию ");
            Sname = Convert.ToString(Console.ReadLine());
            Console.WriteLine("введите Отчество ");
            Otchestvo = Convert.ToString(Console.ReadLine());
            Console.WriteLine("введите Номер телефона ");
            Phone_number = Convert.ToString(Console.ReadLine());
            Console.WriteLine("введите Страну проживания ");
            Country = Convert.ToString(Console.ReadLine());
            Console.WriteLine("введите дату рождения ");
            Birth_date = Convert.ToString(Console.ReadLine());
            Console.WriteLine("введите Название организации");
            Organisation = Convert.ToString(Console.ReadLine());
            Console.WriteLine("введите Должность ");
            Position = Convert.ToString(Console.ReadLine());
            Console.WriteLine("для заметок");
            Other_notes = Convert.ToString(Console.ReadLine());



            foreach (Note note in notebookNotes)
            {
                if (note.Id == Convert.ToUInt32(intValue))
                {
                    note.Id = Convert.ToInt32(intValue);
                    note.Sname = Sname;
                    note.Name = Name;
                    note.Otchestvo = Otchestvo;
                    note.Phone_number = Phone_number;
                    note.Country = Country;
                    note.Birth_date = Birth_date;
                    note.Organisation = Organisation;
                    note.Position = Position;
                    note.Other_notes = Other_notes;
                }

            }

           

           


        }





    }


    
}
